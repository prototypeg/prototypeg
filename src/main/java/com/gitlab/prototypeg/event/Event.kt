/*
 *     ____             __        __                   ______
 *    / __ \_________  / /_____  / /___  ______  ___  / ____/
 *   / /_/ / ___/ __ \/ __/ __ \/ __/ / / / __ \/ _ \/ / __
 *  / ____/ /  / /_/ / /_/ /_/ / /_/ /_/ / /_/ /  __/ /_/ /
 * /_/   /_/   \____/\__/\____/\__/\__, / .___/\___/\____/
 *                                /____/_/
 * @author djdisodo
 * @link https://gitlab.com/djdisodo/prototypeg
 *
 */

package com.gitlab.prototypeg.event

abstract class Event {

	interface Cancellable {
		val isCancelled: Boolean
		fun setCancelled(): Boolean
		open class Base : Event(), Cancellable {
			override var isCancelled = false
				internal set

			override fun setCancelled(): Boolean {
				return false
			}
		}
	}
}
