package com.gitlab.prototypeg.event

import com.gitlab.prototypeg.data.MissionWinResult

open class MissionWinEvent(open var result: MissionWinResult) : Event()