/*
 *     ____             __        __                   ______
 *    / __ \_________  / /_____  / /___  ______  ___  / ____/
 *   / /_/ / ___/ __ \/ __/ __ \/ __/ / / / __ \/ _ \/ / __
 *  / ____/ /  / /_/ / /_/ /_/ / /_/ /_/ / /_/ /  __/ /_/ /
 * /_/   /_/   \____/\__/\____/\__/\__, / .___/\___/\____/
 *                                /____/_/
 * @author djdisodo
 * @link https://gitlab.com/djdisodo/prototypeg
 *
 */

package com.gitlab.prototypeg.doll

import com.gitlab.prototypeg.data.JsonData
import com.gitlab.prototypeg.data.Language
import com.gitlab.prototypeg.data.DollNumber
import com.gitlab.prototypeg.data.Rank
import com.google.gson.JsonObject
import com.google.gson.JsonParser

import java.io.*
import java.util.ArrayList
import java.util.HashMap

open class DollInfo(data: JsonObject) : JsonData(data) {

	var skins: ArrayList<DollNumber>? = null
		get() {
			if (field == null) {
				val skinsData = data[SKINS]?.asJsonArray
				if (skinsData !== null) {
					this.skins = ArrayList()
					for (skin in skinsData) {
						field!!.add(skin.asShort)
					}
				}
			}
			return field
		}
		private set

	open val dollNumber: DollNumber?
		get() = data[DOLL_NUMBER]?.asShort

	open val rank: Rank?
		get() = data[RANK].asByte

	override fun jsonSerialize() {
		//TODO
	}

	open fun getName(language: Language): String {
		return when (language) {
			Language.CN -> cnNames[dollNumber]
			Language.KR -> krNames[dollNumber]
			Language.EN -> enNames[dollNumber]
			Language.JP -> jpNames[dollNumber]
			else -> data[CODENAME].asString
		}!!
	}

	companion object {

		const val DOLL_NUMBER = "id"
		const val RANK = "rank"
		const val SKINS = "skins"
		const val CODENAME = "codename"

		const val NAME_CN = "zh-CN"
		const val NAME_KR = "ko-KR"
		const val NAME_EN = "en-US"
		const val NAME_JP = "ja-JP"


		private val dollData = HashMap<Short, DollInfo>() // 0~

		private val cnNames = HashMap<Short, String>()

		private val krNames = HashMap<Short, String>()

		private val enNames = HashMap<Short, String>()

		private val jpNames = HashMap<Short, String>()

		fun init() {
			try {
				val dollData = DollInfo::class.java.getResourceAsStream("/girlsfrontline-core/data/doll.json")
				val inputStreamReader = InputStreamReader(dollData)
				val bufferedReader = BufferedReader(inputStreamReader)
				val dolls = JsonParser.parseReader(bufferedReader).asJsonArray
				for (element in dolls) {
					val doll = DollInfo(element.asJsonObject)
					this.dollData[doll.dollNumber!!] = doll
				}
				bufferedReader.close()
				inputStreamReader.close()
				dollData.close()
			} catch (e: Exception) {
				e.printStackTrace()
			}

			val loadNames: (String, HashMap<Short, String>) -> Unit = {
				path, output -> run {
				try {
					val dollData = DollInfo::class.java.getResourceAsStream("/girlsfrontline-core/i18n/" + path + "/gun.json")
					val inputStreamReader = InputStreamReader(dollData)
					val bufferedReader = BufferedReader(inputStreamReader)
					val dolls = JsonParser.parseReader(bufferedReader).asJsonObject
					for (key in dolls.keySet()) {
						if (key.startsWith("gun-1")) {
							output[key.substring(5).toShort()] = dolls[key].asString
						}
					}
					bufferedReader.close()
					inputStreamReader.close()
					dollData.close()
				} catch (e: Exception) {
					e.printStackTrace()
				}
				}
			}
			loadNames.invoke(NAME_CN, cnNames)
			loadNames.invoke(NAME_EN, enNames)
			loadNames.invoke(NAME_JP, jpNames)
			loadNames.invoke(NAME_KR, krNames)
		}

		operator fun get(dollNumber: DollNumber?): DollInfo {
			return dollData[dollNumber]?: UnknownDollInfo(dollNumber)
		}
	}
}
