/*
 *     ____             __        __                   ______
 *    / __ \_________  / /_____  / /___  ______  ___  / ____/
 *   / /_/ / ___/ __ \/ __/ __ \/ __/ / / / __ \/ _ \/ / __
 *  / ____/ /  / /_/ / /_/ /_/ / /_/ /_/ / /_/ /  __/ /_/ /
 * /_/   /_/   \____/\__/\____/\__/\__, / .___/\___/\____/
 *                                /____/_/
 * @author djdisodo
 * @link https://gitlab.com/djdisodo/prototypeg
 *
 */

package com.gitlab.prototypeg

import com.gitlab.prototypeg.doll.Doll
import com.gitlab.prototypeg.event.*
import com.gitlab.prototypeg.network.Handler
import com.gitlab.prototypeg.network.response.*

internal class MainResponseHandler(private val session: Session) : Handler<Response> {

	@Handler.Synchronized
	fun handleSignResponse(signResponse: SignResponse) {
		val signChangeEvent = SignChangeEvent(signResponse.sign!!)
		session.eventManager.call(signChangeEvent)
		if (!signChangeEvent.isCancelled) {
			session.networkManager.sign = signResponse.sign!!
		}
	}

	fun handleIndexResponse(indexResponse: IndexResponse) {
		session.index = indexResponse
	}

	fun handleBuildDollResponse(buildDollResponse: BuildDollResponse) {
		val dollBuildEvent = DollBuildEvent(buildDollResponse.dollId!!)
		session.eventManager.call(dollBuildEvent)
	}

	fun handleBattleFinishResponse(battleFinishResponse: BattleFinishResponse) {
		val rewardDolls = battleFinishResponse.rewardDolls
		if (rewardDolls !== null) {
			for (dollWithUser in rewardDolls) {
				val dollGetEvent = DollGetEvent(Doll(
						session.index!!.userInfo!!.userId!!,
						dollWithUser.dollId!!,
						dollWithUser.dollNumber!!
				))
				session.eventManager.call(dollGetEvent)
				if (!dollGetEvent.isCancelled) {
					//TODO add doll
				}
			}
		}
	}

	fun handleEndTurnResponse(endTurnResponse: EndTurnResponse) {
		val missionWinResult = endTurnResponse.missionWinResult
		if ((missionWinResult !== null) && (missionWinResult.data.size() != 0)) {
			val missionWinEvent = MissionWinEvent(missionWinResult)
			session.eventManager.call(missionWinEvent)
			val rewardDolls = missionWinEvent.result.rewardDolls
			if (rewardDolls !== null) {
				for (dollWithUser in rewardDolls) {
					val dollGetEvent = DollGetEvent(Doll(
							session.index!!.userInfo!!.userId!!,
							dollWithUser.dollId!!,
							dollWithUser.dollNumber!!
					))
					session.eventManager.call(dollGetEvent)
					if (!dollGetEvent.isCancelled) {
						//TODO add doll
					}
				}
			}
		}
	}

	fun onFriendVisit(friendVisitResponse: FriendVisitResponse) {
		session.eventManager.call(FriendVisitEvent(friendVisitResponse))
	}
}
