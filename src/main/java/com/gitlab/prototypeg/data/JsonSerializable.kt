/*
 *     ____             __        __                   ______
 *    / __ \_________  / /_____  / /___  ______  ___  / ____/
 *   / /_/ / ___/ __ \/ __/ __ \/ __/ / / / __ \/ _ \/ / __
 *  / ____/ /  / /_/ / /_/ /_/ / /_/ /_/ / /_/ /  __/ /_/ /
 * /_/   /_/   \____/\__/\____/\__/\__, / .___/\___/\____/
 *                                /____/_/
 * @author djdisodo
 * @link https://gitlab.com/djdisodo/prototypeg
 *
 */

package com.gitlab.prototypeg.data

import com.google.gson.JsonElement
import com.google.gson.JsonObject
import kotlin.reflect.KFunction1

interface JsonSerializable<T : JsonElement> {

	var data: T

	fun jsonSerialize()

	fun <V: JsonData> get(key: String, factory: (data: JsonObject) -> V): V? {
		val jsonElement = data.asJsonObject?.get(key)
		return if (jsonElement === null) {
			null
		} else if (!jsonElement.isJsonObject  && (jsonElement.asJsonArray.size() == 0)) {
			null
		} else {
			factory.invoke(jsonElement.asJsonObject)
		}
	}

	fun set(key: String, jsonData: JsonData?) {
		jsonData?.jsonSerialize()
		(data as? JsonObject)?.add(key, jsonData?.data)
	}
}
