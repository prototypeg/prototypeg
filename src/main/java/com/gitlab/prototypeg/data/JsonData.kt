/*
 *     ____             __        __                   ______
 *    / __ \_________  / /_____  / /___  ______  ___  / ____/
 *   / /_/ / ___/ __ \/ __/ __ \/ __/ / / / __ \/ _ \/ / __
 *  / ____/ /  / /_/ / /_/ /_/ / /_/ /_/ / /_/ /  __/ /_/ /
 * /_/   /_/   \____/\__/\____/\__/\__, / .___/\___/\____/
 *                                /____/_/
 * @author djdisodo
 * @link https://gitlab.com/djdisodo/prototypeg
 *
 */

package com.gitlab.prototypeg.data

import com.google.gson.JsonObject

abstract class JsonData(data: JsonObject?) : JsonSerializable<JsonObject> {
	final override var data: JsonObject

	init {
		if (data == null) {
			this.data = JsonObject()
		} else {
			this.data = data
		}
	}

	override fun jsonSerialize() {}

}
