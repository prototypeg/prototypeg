package com.gitlab.prototypeg.network.response

import com.gitlab.prototypeg.network.request.Request
import com.google.gson.JsonObject

abstract class JsonObjectResponse(buffer: ByteArray, request: Request) : JsonResponse<JsonObject>(buffer, request) {
	override var data: JsonObject = JsonObject()
		get() {
			if (!this.isDecoded) {
				this.decode(this.decodeSign)
			}
			return field
		}
}