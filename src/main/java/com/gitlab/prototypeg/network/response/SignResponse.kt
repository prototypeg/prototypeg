/*
 *     ____             __        __                   ______
 *    / __ \_________  / /_____  / /___  ______  ___  / ____/
 *   / /_/ / ___/ __ \/ __/ __ \/ __/ / / / __ \/ _ \/ / __
 *  / ____/ /  / /_/ / /_/ /_/ / /_/ /_/ / /_/ /  __/ /_/ /
 * /_/   /_/   \____/\__/\____/\__/\__, / .___/\___/\____/
 *                                /____/_/
 * @author djdisodo
 * @link https://gitlab.com/djdisodo/prototypeg
 *
 */

package com.gitlab.prototypeg.network.response

import com.gitlab.prototypeg.data.UserId
import com.gitlab.prototypeg.gfencryption.Sign
import com.gitlab.prototypeg.network.NetworkManager
import com.gitlab.prototypeg.network.request.Request

import java.io.IOException

open class SignResponse(buffer: ByteArray, request: Request) : JsonObjectResponse(buffer, request) {

	open var userId: UserId?
		get() = data[USER_ID].asInt
		set(userId) = this.data.addProperty(USER_ID, userId.toString())

	open var sign: Sign?
		get() = Sign(data[SIGN]?.asString)
		set(sign) = data.addProperty(SIGN, sign.toString())

	open var isUsernameExist: Boolean?
		get() = data[USERNAME_EXIST]?.asBoolean
		set(usernameExist) = data.addProperty(USERNAME_EXIST, usernameExist)

	open var appGuardId: String?
		get() = data[APP_GUARD_ID]?.asString
		set(appGuardId) = data.addProperty(APP_GUARD_ID, appGuardId)

	open var isRealName: Boolean?
		get() = data[IS_REAL_NAME]?.asByte?.toInt() != 0
		set(realName) = data.addProperty(IS_REAL_NAME, if (realName?: false) 0 else 1)

	open var authenticationUrl: String?
		get() = data[AUTHENTICATION_URL]?.asString
		set(authenticationUrl) = data.addProperty(AUTHENTICATION_URL, authenticationUrl)

	@Throws(IOException::class)
	override fun encode(sign: Sign?) {
		super.encode(NetworkManager.INITIAL_SIGN)
	}

	@Throws(IOException::class)
	override fun decode(sign: Sign?) {
		super.decode(NetworkManager.INITIAL_SIGN)
	}

	companion object {
		const val USER_ID = "uid"
		const val SIGN = "sign"
		const val USERNAME_EXIST = "is_username_exist"
		const val APP_GUARD_ID = "app_guard_id"
		const val IS_REAL_NAME = "real_name"
		const val AUTHENTICATION_URL = "authentication_url"
	}
}
