package com.gitlab.prototypeg.network.response

import com.gitlab.prototypeg.network.request.Request

open class FriendVisitResponse(buffer : ByteArray, request : Request) : JsonObjectResponse(buffer, request) {

	open var buildCoinCount : Byte?
		get() = data[BUILD_COIN_COUNT].asByte
		set(value) = data.addProperty(BUILD_COIN_COUNT, value)

	companion object {
		const val INFO = "info"
		const val ADJUTANT_LIST = "adjutant_list"
		const val SANGVIS_WITH_USER_LIST = "sangvis_with_user_list"
		const val GUN_WITH_USER_LIST = "gun_with_user_list"
		const val DORM_TYPE = "dorm_type"
		const val FURNITURE_LIST = "furniture_list"
		const val BUILD_COIN_COUNT = "build_coin_flag"
	}
}