/*
 *     ____             __        __                   ______
 *    / __ \_________  / /_____  / /___  ______  ___  / ____/
 *   / /_/ / ___/ __ \/ __/ __ \/ __/ / / / __ \/ _ \/ / __
 *  / ____/ /  / /_/ / /_/ /_/ / /_/ /_/ / /_/ /  __/ /_/ /
 * /_/   /_/   \____/\__/\____/\__/\__, / .___/\___/\____/
 *                                /____/_/
 * @author djdisodo
 * @link https://gitlab.com/djdisodo/prototypeg
 *
 */

package com.gitlab.prototypeg.network.response


import com.gitlab.prototypeg.data.JsonSerializable
import com.gitlab.prototypeg.gfencryption.*
import com.gitlab.prototypeg.network.request.Request
import com.google.gson.JsonElement
import com.google.gson.JsonObject
import com.google.gson.JsonParser
import com.google.gson.JsonPrimitive
import org.jetbrains.annotations.Nullable


import java.io.*
import java.util.Date

abstract class JsonResponse<T : JsonElement>(buffer: ByteArray, request: Request) : Response(buffer, request), JsonSerializable<T> {
	open var date = Date()

	@Throws(IOException::class)
	override fun encode(sign: Sign?) {
		if (sign == null) {
			throw IOException(Sign::class.java.toString() + " is required to encode")
		}
		this.jsonSerialize()
		val byteArrayOutputStream = ByteArrayOutputStream()
		byteArrayOutputStream.write(byteArrayOf('#'.toByte()))
		byteArrayOutputStream.flush()
		val outputStream = GfEncryptionOutputStream(byteArrayOutputStream)
		val writer = OutputStreamWriter(outputStream)
		writer.write(data!!.toString())
		writer.flush()
		outputStream.finish(date, sign, true)
		outputStream.flush()
		this.buffer = byteArrayOutputStream.toByteArray()
		writer.close()
		outputStream.close()
		byteArrayOutputStream.close()
		super.encode(sign)
	}

	@Throws(IOException::class)
	override fun decode(sign: Sign?) {
		if (sign == null) {
			throw IOException(Sign::class.java.toString() + " is required to decode")
		}
		if (buffer == null) {
			throw IOException("null buffer")
		} else if (buffer!![0] != '#'.toByte()) {
			throw IOException("not a JsonResponse")
		}
		val byteArrayInputStream = ByteArrayInputStream(buffer!!, 1, buffer!!.size - 1)
		val inputStream = GfEncryptionInputStream(
				byteArrayInputStream,
				sign
		)
		val bufferedReader = BufferedReader(
				InputStreamReader(inputStream)
		)
		date = inputStream.date
		this.jsonDeserialize(bufferedReader)
		bufferedReader.close()
		inputStream.close()
		byteArrayInputStream.close()
		super.decode(sign)
	}

	override fun jsonSerialize() {}

	@Throws(IOException::class)
	protected open fun jsonDeserialize(reader: BufferedReader) {
		data = JsonParser.parseReader(reader) as T
	}
}
