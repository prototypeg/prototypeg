/*
 *     ____             __        __                   ______
 *    / __ \_________  / /_____  / /___  ______  ___  / ____/
 *   / /_/ / ___/ __ \/ __/ __ \/ __/ / / / __ \/ _ \/ / __
 *  / ____/ /  / /_/ / /_/ /_/ / /_/ /_/ / /_/ /  __/ /_/ /
 * /_/   /_/   \____/\__/\____/\__/\__, / .___/\___/\____/
 *                                /____/_/
 * @author djdisodo
 * @link https://gitlab.com/djdisodo/prototypeg
 *
 */

//incomplete
package com.gitlab.prototypeg.network.request

import com.gitlab.prototypeg.data.DollId
import com.gitlab.prototypeg.data.DollWithUser
import com.gitlab.prototypeg.data.JsonData
import com.gitlab.prototypeg.network.response.BattleFinishResponse
import com.google.gson.JsonObject
import com.google.gson.JsonArray
import java.util.*

open class BattleFinishRequest(buffer: ByteArray, path: String): DataRequest(buffer, path) {

    open var leftHealthOfDolls: MutableList<DollBattleResultInfo>? = null
        get() {
            if (field == null) {
                val dolls = data.getAsJsonArray(DOLLS)
                if (dolls != null) {
                    leftHealthOfDolls = ArrayList()
                    for (element in dolls) {
                        field!!.add(Companion.DollBattleResultInfo(element as JsonObject))
                    }
                }
            }
            return field
        }

    override fun jsonSerialize() {
        super.jsonSerialize()
        val dolls = JsonArray()
        for (doll in leftHealthOfDolls!!) {
            dolls.add(doll.data)
        }
        data.add(DOLLS, dolls)
    }

    companion object {
        const val DOLLS = "guns"

        open class DollBattleResultInfo(data: JsonObject?) : JsonData(data) {
            open var dollId: DollId?
                get() = data.get("id")?.asInt
                set(dollId) = data.addProperty("id", dollId)
            open var life: Int?
                get() = data.get("life")?.asInt
                set(dollId) = data.addProperty("life", dollId)
        }
    }
}