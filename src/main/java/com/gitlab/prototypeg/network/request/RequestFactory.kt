/*
 *     ____             __        __                   ______
 *    / __ \_________  / /_____  / /___  ______  ___  / ____/
 *   / /_/ / ___/ __ \/ __/ __ \/ __/ / / / __ \/ _ \/ / __
 *  / ____/ /  / /_/ / /_/ /_/ / /_/ /_/ / /_/ /  __/ /_/ /
 * /_/   /_/   \____/\__/\____/\__/\__, / .___/\___/\____/
 *                                /____/_/
 * @author djdisodo
 * @link https://gitlab.com/djdisodo/prototypeg
 *
 */

package com.gitlab.prototypeg.network.request

import java.util.HashMap

typealias RequestCaster = (ByteArray, String) -> Request

interface RequestFactory {

	companion object {
		private val requestCasters = HashMap<String, RequestCaster>()

		operator fun get(path: String, buffer: ByteArray): Request {
			val requestCaster = requestCasters[path]
			if (requestCaster == null) {
				val unknownParamRequest = UnknownParamRequest(buffer, path)
				return if (unknownParamRequest.params!!.containsKey("outdatacode")) {
					UnknownDataRequest(buffer, path)
				} else {
					unknownParamRequest
				}
			}
			return requestCaster.invoke(buffer, path)
		}

		fun registerRequest(path: String, requestFactory: RequestCaster) {
			requestCasters[path] = requestFactory
		}

		fun init() {
			registerRequest("Mission/battleFinish", ::BattleFinishRequest);
		}
	}
}
