/*
 *     ____             __        __                   ______
 *    / __ \_________  / /_____  / /___  ______  ___  / ____/
 *   / /_/ / ___/ __ \/ __/ __ \/ __/ / / / __ \/ _ \/ / __
 *  / ____/ /  / /_/ / /_/ /_/ / /_/ /_/ / /_/ /  __/ /_/ /
 * /_/   /_/   \____/\__/\____/\__/\__, / .___/\___/\____/
 *                                /____/_/
 * @author djdisodo
 * @link https://gitlab.com/djdisodo/prototypeg
 *
 */

package com.gitlab.prototypeg.network
import com.gitlab.prototypeg.Session
import com.gitlab.prototypeg.event.DecodeFailedEvent

import java.io.IOException
import java.lang.reflect.Method
import java.util.ArrayList
import java.util.LinkedHashMap
import java.util.concurrent.ExecutionException
import java.util.concurrent.FutureTask

open class HandlerManager<T : Decodable>(private val networkManager:NetworkManager, private val decodable:Class<T>) {

	private val handlers = LinkedHashMap<Handler<T>, ArrayList<Method>>()
	private val synchronizedHandlers = LinkedHashMap<Handler<T>, ArrayList<Method>>()

	open fun registerHandler(handler:Handler<T>) {
		val handlerMethods = ArrayList<Method>()
		val synchronizedHandlerMethods = ArrayList<Method>()

		handler.javaClass.methods.forEach {
			it.isAccessible = true
			if (it.parameterCount == 1 && decodable.isAssignableFrom(it.parameterTypes[0])) {
				if (it.isAnnotationPresent(Handler.Synchronized::class.java)) {
					synchronizedHandlerMethods
				} else {
					handlerMethods
				}.add(it)
			}
		}
		if (handlerMethods.size != 0) {
			handlers[handler] = handlerMethods
		}

		if (synchronizedHandlerMethods.size != 0) {
			synchronizedHandlers[handler] = synchronizedHandlerMethods
		}
	}

	open fun unregisterHandler(handler:Handler<T>) {
		handlers.remove(handler)
		synchronizedHandlers.remove(handler)
	}


	open fun handle(decodable:T) {
		val tasks = ArrayList<HandlerMethodInvokeTask>()
		decodable.decodeSign = this.networkManager.sign
		if (synchronizedHandlers.size != 0) {
			synchronizedHandlers.forEach { (handler, methods) -> for (method in methods) {
				if (method.parameterTypes[0].isInstance(decodable)) {
					tasks.add(HandlerMethodInvokeTask(handler, method, decodable))
				}
			}}
			tasks.forEach {
				networkManager.session.executorService.submit(it)
			}
			for (task in tasks) {
				try {
					task.get()
				} catch (e:InterruptedException) {
					e.printStackTrace()
				} catch (e:ExecutionException) {
					e.printStackTrace()
				}
			}
			if (decodable.isEdited) {
				try {
					decodable.encode(networkManager.sign)
				} catch (e:IOException) {
					Session.LOGGER.info("encode failed")
				}
			}
		}
		networkManager.session.executorService.submit {
			handlers.forEach { (handler, methods) -> for (method in methods) {
				if (method.parameterTypes[0].isInstance(decodable)) {
					networkManager.session.executorService.submit(HandlerMethodInvokeTask(handler, method, decodable))
				}
			}}
		}
	}

	private inner class HandlerMethodInvokeTask(handler:Handler<T>, method:Method, decodable:T): FutureTask<T>({ try
	{
		method.invoke(handler, decodable)
	}
	catch (e:Throwable) {
		e.printStackTrace()
		Session.LOGGER.info("Unregistering Handler:" + handler.javaClass.name)
		this@HandlerManager.unregisterHandler(handler)
	}
	}, null)
}
